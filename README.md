# JST-GH CAN Transceiver Breakout Board

Behold, a simple and cheap CAN transceiver breakout board. This little breakout board should meet all of your CAN transceiving needs:

* Classic CAN and CAN-FD support
* 5V and 3V3 MCU support
* Automotive-grade CAN transceiver
* JST-GH 4 pin connector for connecting to most Pixhawk® family autopilots
* Testpoints for debugging without JST-GH
* Silent mode
* Terminator (enabled using solder bridge)
* Small form factor
* Cheap (~$3 USD in parts)
* Easy to make

The transceiver used is the NXP TJA1051-TK/3,118 automotive CAN/CAN-FD transceiver. It is one of the recommended chips for use by the Pixhawk® connector standards.

Designed using KiCad. Developed by Kalyan Sriram and the AmadorUAVs team.

# Usage
## JST-GH
The JST-GH side is self explanatory - plug in the CAN cables into both connectors to continue the chain.

## Pins
Wire 5V and GND, as well as CAN RX (CRX) and CAN TX (CTX) to the MCU. Connect VIO to the native logic voltage of your MCU (3V3 on most STM32 series processors). To enable transmitting data on the transceiver, you will also have to connect the SILENT (SLNT) pin to ground. This pin is pulled-up by the board, which makes the transceiver operate in silent mode by default (listens but doesn't transmit). See the datasheet for the NXP TJA1051-TK/3,118 for more details. I recommend wiring SLNT to a GPIO port and pulling that down in code, so that if your MCU is not working/running, the transceiver falls back to silent mode by default. If you don't want the fuss, just ground the pin.

## Terminator
The breakout board includes a 120 Ohm CAN terminator for more convenient CAN-bus termination. To use this, (don't plug in the second JST-GH cable) and bridge the solder bridge labeled "TERM".

## Power via CAN
The board also supports powering the MCU via CAN, or powering the CAN bus via the MCU. To enable this, bridge the solder bridge labeled "PWR". A few things to keep in mind with this:
* Don't overload the CAN-bus 5V line, so if your MCU takes a lot of power, consider a different power source.
* If you're powering the CAN bus via the MCU, make sure there aren't any other conflicting power sources on the line.
* There is ESD protection on the 5V line, but no reverse polarity protection (not a problem unless you've done something to your CAN cables or another node is broken).

# Pictures
![Top](img/top.png)

![Side](img/angled.png)

![Bottom](img/bottom.png)
